<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <img src='https://e.akademicivil.com/resim/20535.png' style="width:75%" />
    </a>
    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="./">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Anasayfa</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Menü
    </div>

    <!-- Nav Item - Tables -->
    <li class="nav-item">
        <?php

        $yetkiSeviyesi = $_SESSION["rutbe"];
        $singleMenu = $db->qr("      
                select * from menulist
                inner join menuauthority on menuauthority.menuid=menulist.id
                where  menulist.grupId is null  and menuauthority.userId = '{$yetkiSeviyesi}'
               ");
        while ($singleRow = $singleMenu->fetch(PDO::FETCH_ASSOC)) {
            echo ' <a class="nav-link" href="/' . $singleRow["menuSelf"] . '">  <i class="' . $singleRow["icon"] . '"></i>
                                        <span>' . $singleRow["menuAd"] . '</span></a>';
        }
        ?>
    </li>

    <?php

    $yetkiSeviyesi = $_SESSION["rutbe"];
    $menuSeviyesi = $db->qr("
                select distinct *,grupmenu.id as gropId from grupmenu 
                inner join groupauthority on groupauthority.group_id=grupmenu.id
                where groupauthority.authority = '$yetkiSeviyesi'
            ");
    foreach ($menuSeviyesi as $menuKey => $menuValue) {

        echo '
              
                <li class="nav-item">
                    <a class="nav-link" href="#" data-toggle="collapse" data-target="#grup' . $menuValue["id"] . '" aria-expanded="false" aria-controls="collapseTwo">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>' . $menuValue["name"] . '</span>
                    </a>
                    <div id="grup' . $menuValue["id"] . '" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                    <ul class="list-group menu_container">
            ';

        $menuGrup = $db->qr("
                    select * from menulist
                    inner join menuauthority on menuauthority.menuid=menulist.id
                    where menulist.grupId = '{$menuValue["gropId"]}' and menuauthority.userId = '{$yetkiSeviyesi}'  order by orderAZ desc
                    
            ");

        foreach ($menuGrup as $grupKey => $grupVal) {


            echo '
                <li class="list-group-item menu">
                <a class="nav-collapse-item" href="./' . $grupVal["menuSelf"] . '">  <i class="' . $grupVal["icon"] . '"></i>
                        ' . $grupVal["menuAd"] . '</a>
                        </li>
                ';
        }

        echo '
            </ul>
            </div>
            </div>
                </li>
    
                ';
    }
    ?>


    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>