<?php
$page = $_GET["page"];
$id = $_SESSION["id"];
$pExp = explode(".", $page);
@$pId = $pExp[1];
$rutbe = $_SESSION["rutbe"];
if ($rutbe == 5 || $rutbe == 1 || $rutbe == 2) {
    if (is_numeric($pId)) {
        $sql = $db->qr("select *,DATE_FORMAT(Created_date,'%d/%m/%Y %H:%i') as tarih from ordermember where id = '$pId'");
        if ($sql->rowCount() == 1) {
            $sonuc = $sql->fetch(PDO::FETCH_ASSOC);
            $kadi = $sonuc["firstlastName"];
            $magaza = $sonuc["OfficeDesc"];
            $cityTown = $sonuc["city"] . "/" . $sonuc["town"];
            $phone = $sonuc["phone1"];
            $tarih = $sonuc["tarih"];
            ?>
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Sipariş Numarası : <?=$pId?></h1>
                </div>
                <div class="row mb-5">

                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-2 col-md-5 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Adı Soyadı</div>
                                        <div class="h6 mb-0 font-weight-bold text-gray-800"><?=$kadi?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-user text-primary fa-2x"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-2 col-md-5 mb-4">
                        <div style="border-left:.25rem solid #e74825 !important" class="card shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div style="color:#e74825" class="text-xs font-weight-bold text-uppercase mb-1">Mağaza</div>
                                        <div  style="color:#e74825" class="h6 mb-0 font-weight-bold "><?=$magaza?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i style="color:#e74825" class="fas fa-store fa-2x"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-2 col-md-5 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Şehir</div>
                                        <div class="h6 mb-0 font-weight-bold text-gray-800"><?=$cityTown?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-home fa-2x text-success"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-2 col-md-5 mb-4">
                        <div class="card border-left-info shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Cep Telefon Numarası</div>
                                        <div class="row no-gutters align-items-center">

                                            <div class="h6 mb-0 font-weight-bold text-gray-800">0<?=$phone?></div>

                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-phone fa-2x text-info"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Pending Requests Card Example -->
                    <div class="col-xl-2 col-md-5 mb-4">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Tarih</div>
                                        <div class="h6 mb-0 font-weight-bold text-gray-800"><?=$sonuc["tarih"]?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-calendar fa-2x text-warning"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-lg-12">


                        <table class="table table-striped table-inverse text-center  " style="color:#000">
                            <thead class="">
                                <tr>
                                    <th>Ürün Kodu</th>
                                    <th>Ürün Adı</th>
                                    <th>Miktar</th>

                                </tr>
                            </thead>
                            <?php
$qr = $db->qr("select * from orderproduct where omId = '$pId'");

            ?>
                            <tbody>
                                <?php

            while ($rw = $qr->Fetch(PDO::FETCH_ASSOC)) {

                echo '
                                <tr>
                                <td>' . $rw["productCode"] . '</td>
                                    <td>' . $rw["productName"] . '</td>
                                    <td>1</td>

                                </tr>
                                ';
            }

            ?>
                            </tbody>
                        </table>

                    </div>


                </div>

                <div class="row mt-5">
                <?php
$tipi = $sonuc["Assem"];

$tipiOut = $db->qr("select * from montajtipi where id = '$tipi'")->fetch(PDO::FETCH_ASSOC);

            if ($sonuc["Assem"] == 1) {

                echo '
                    <div class="col-xl-2 col-md-5 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Montaj Olacak mı ?</div>
                                        <div class="h6 mb-0 font-weight-bold text-gray-800">'.$tipiOut["name"].'</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa fa-wrench fa-2x text-success"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    ';

            }elseif  ($sonuc["Assem"] == 2) {
                
           
                


                echo '
                    <div class="col-xl-2 col-md-5 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Montaj Olacak mı ?</div>
                                        <div class="h6 mb-0 font-weight-bold text-gray-800">'.$tipiOut["name"].'</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa fa-wrench fa-2x text-success"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    ';

            } else {

                echo '
                <div class="col-xl-2 col-md-5 mb-4">
                    <div class="card border-left-warning shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Montaj Olacak mı ?</div>
                                    <div class="h6 mb-0 font-weight-bold text-gray-800">Hayır</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fa fa-wrench fa-2x text-warning"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                ';
            }

            $kargoTakip = $db->qr("select * from kargofirma where urunId = '$pId'");
            if ($kargoTakip->rowCount() == 1) {

                $kargoCikti = $kargoTakip->fetch(PDO::FETCH_ASSOC);

                ?>

                    <div class="col-xl-2 col-md-5 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Sipariş Durumu</div>
                                    <?php
                                    if ($kargoCikti["kargoDurumu"] == 1) {
                                        echo "<div class='h6 mb-0 font-weight-bold text-gray-800'>Ürün Alındı</div>";
                                    } elseif ($kargoCikti["kargoDurumu"] == 2) {
                                        echo "<div class='h6 mb-0 font-weight-bold text-gray-800'>Ürün Hazırlanıyor</div>";
                                    } elseif ($kargoCikti["kargoDurumu"] == 3) {
                                        echo "<div class='h6 mb-0 font-weight-bold text-gray-800'>Ürün Kargoya</div>";
                                    } elseif($kargoCikti["kargoDurumu"] == 4){
                                        echo "<div class='h6 mb-0 font-weight-bold text-gray-800'>Sipariş Tamamalandı <i class='fas fa-award'></i></div>";
                                    } else {
                                        echo "<div class='h6 mb-0 font-weight-bold text-gray-800'>Sipariş İşleme Alınmamıştır</div>";
                                    }
                                    ?>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa fa-check fa-2x text-success"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            <?php
if ($kargoCikti["kargoDurumu"] == 3) {

                    ?>

                    <div class="col-xl-2 col-md-5 mb-4">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Kargo Takip Numarası</div>
                                        <div class="h6 mb-0 font-weight-bold text-gray-800">4e8a9e8d</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa fa-truck-moving fa-2x text-warning"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php

                }
            } else {

                ?>
<div class="col-xl-2 col-md-5 mb-4">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Sipariş Durumu</div>
                                        <div class='h6 mb-0 font-weight-bold text-gray-800'>Sipariş İşleme Alınmamıştır</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa fa-spinner fa-2x text-warning"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
}

            if ($sonuc["Assem"] == 1 || $sonuc["Assem"] == 2) {

                $pd = $db->qr("select * from montaj where urun_id = '$pId'")->fetch(PDO::FETCH_ASSOC);

                if ($pd["durum"] == 1) {

                    ?>

    <div class="col-xl-2 col-md-5 mb-4">
    <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Montaj Durumu</div>
                    <div class='h6 mb-0 font-weight-bold text-gray-800'>Montajı Yapıldı</div>
                </div>
                <div class="col-auto">
                    <i class="fa fa-cog fa-2x text-success"></i>
                </div>
            </div>
        </div>
    </div>
</div>

    <?php
} else {

                    ?>

<div class="col-xl-2 col-md-5 mb-4">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Montaj Durumu</div>
                                        <div class='h6 mb-0 font-weight-bold text-gray-800'>Montajı Beklemede</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa fa-cog fa-2x text-warning"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<?php
}

            }
            
    if($rutbe == 2){

            ?>

            <div class="col-xl-2 mb-4 d-sm-none d-md-none d-xl-inline">
            <a href='urunlog.<?=$pId?>'>
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="h6 mb-0 font-weight-bold text-gray-800">Ürün Hareketi</div>
                            </div>
                            <div class="col-auto">
                                <i class="fa fa-paper-plane fa-2x text-success"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            </div>
            <?php

}
            ?>
                    <div class="col-xl-2 mb-4 d-sm-none d-md-none d-xl-inline">
                    <a href='invoice.php?print=a9358f4ce02708d5b5a2a70818b2a6da.<?=base64_encode($pId)?>'>
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="h6 mb-0 font-weight-bold text-gray-800">Çıktı Al</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa fa-print fa-2x text-success"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    </div>
                </div>
<?php

        } else {
            ?>

<div class="d-flex justify-content-center align-items-center" id="main">
    <h1 class="mr-3 pr-3 align-top border-right inline-block align-content-center">Hata</h1>
    <div class="inline-block align-middle">
    	<h2 class="font-weight-normal lead" id="desc">Sonuç Bulunamadı</h2>
    </div>
</div>
<?php
header("refresh:1;url=./");
        }
    }
}else{

    echo '
        
<div class="d-flex justify-content-center align-items-center" id="main">
<h1 class="mr-3 pr-3 align-top border-right inline-block align-content-center">Hata</h1>
<div class="inline-block align-middle">
    <h2 class="font-weight-normal lead" id="desc">Sonuç Bulunamadı</h2>
</div>
</div>
    ';

}
?>