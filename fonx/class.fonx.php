<?php

    class ayar{


        public $x;

        public function __construct(){

          return  $this->x = new PDO("mysql:host=localhost;dbname=firmatakip","root","root",[
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'set charset utf8'
            ]);
        }

        public function call($call){
            return $this->x->query("call $call");
        }

        public function lastId(){
            return $this->x->lastInsertId();
        }

        public function post($post){
            return htmlspecialchars(strip_tags(trim($post)));
        }

        public function qr($sq){
            return $this->x->query($sq);
        }

        public function mail($mail,$mail_gonder,$isim_soyisim,$body,$siparisNo){

            $mailKul = $this->qr("select * from mail")->fetch(PDO::FETCH_ASSOC);

            $mail->isSMTP();                                            
            $mail->Host       = 'smtp.gmail.com';                    
            $mail->SMTPAuth   = true;                                   
            $mail->Username   = $mailKul["kadi"];                     
            $mail->Password   = $mailKul["sifre"];                               
            $mail->Port       = 587; 
            $mail->CharSet = 'UTF-8';
            

            $mail->setFrom($mail_gonder, $isim_soyisim);
            $mail->addAddress($mail_gonder, $isim_soyisim);
            $mail->isHTML(true);
            $mail->Subject = 'Civil Mağazacılık A.Ş. Sipariş No : '.$siparisNo;
            $mail->Body    = $body;

    $mail->AltBody = '';

    return $mail->send();

        }
    }

