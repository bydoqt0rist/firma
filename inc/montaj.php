
                    
                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Montaj</h1>


                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary"></h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Sipariş No</th>
                                            <th>Mağaza</th>
                                            <th>Adı Soyadı</th>
                                            <th>İl</th>
                                            <th>İlçe</th>
                                            <th>Cep Telefon Numarası</th>
                                            <th>Tarih</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php

$db->qr("SET lc_time_names='tr_TR';");
$qr = $db->qr("select *,DATE_FORMAT(Created_Date,'%d %M %Y') as tarih from ordermember where Assem = 1 order by id desc ");

while ($rw = $qr->fetch(PDO::FETCH_ASSOC)) {

    echo '
                                        <tr>
                                        <td>' . $rw["id"] . '</td>
                                        <td>' . $rw["OfficeDesc"] . '</td>
                                        <td>' . $rw["firstlastName"] . '</td>
                                        <td>' . $rw["city"] . '</td>
                                        <td>' . $rw["town"] . '</td>
                                        <td>' . $rw["phone1"] . '</td>
                                        <td>' . $rw["tarih"] . '</td>
                                        <td>
                                            <a href="./cardMontaj.' . $rw["id"] . '"><i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                        ';
}

?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
