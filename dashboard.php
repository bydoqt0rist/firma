<?php
ob_start();
session_Start();
require 'fonx/src/Exception.php';
require 'fonx/src/PHPMailer.php';
require 'fonx/src/SMTP.php';


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once "fonx/class.fonx.php";
require_once "fonx/class.uye.php";
require_once "fonx/class.urun.php";
require_once "fonx/class.curl.php";


$db = new ayar();
$curl = new curl();
$mail = new PHPMailer();


if ($_SESSION["login"] == 1) {

    ?>
<!DOCTYPE html>
<html lang="tr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Civil Mağazacılık A.Ş.">

    <title><?php
    @$yetki = $_SESSION["rutbe"];
    switch ($yetki):
        case 1:
            echo "System Admini Hoş Geldiniz";
            break;
        case 2:
            echo "Yetkili Girişi";
        break;
        case 3:
            echo "Firma Girişi";
        break;
        case 4:
            echo "Montaj Girişi";
        break;
        case 5:
            echo "Müşteri Girişi";
        break;
            default:
            echo "Hoş Geldiniz";
            endswitch;

    ?></title>    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="css/custom.css" rel="stylesheet" media="screen" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <script src="vendor/jquery/jquery.min.js"></script>
    
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

    <script>
        function ajax() {
            $.ajax({
                method: "post",
                url: "ajax.php",
                data: "ekle",
                success: function(e) {
                    $("#add").append(e);
                }
            })
        }
        $(document).ready(function() {

            $("#item_add").click(function() {
                ajax();
            });
        })
    </script>
</head>

<body id="page-top">
    <div id="wrapper">

        <!-- Sidebar -->
        <?php
            require_once "inc/sidebar.php";
        ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Search -->

                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php
                                    $ul = $_SESSION["ulName"];
                                    echo $ul;
                                ?></span>
                                <span class="bg-info text-white rounded-circle" style="padding:5px 10px;"><?=
                                    strtoupper(substr($ul,0,1));?></span>
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                         
                                <div class="dropdown-divider"></div>
                                <a class="fa-sm fa-fw mr-2 ml-4 logout" href="./cikis">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 "></i> Çıkış Yap
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <div class="container-fluid">


                    <div class="row">

                        <div class="col-lg-12 mb-4">

                <?php


    $page = @$_GET["page"];
    $parca = explode(".",$page);
    
    
    if (file_exists("inc/" . $parca[0] . ".php") == "") {
        require_once "inc/anasayfa.php";
    } else {
        require_once "inc/" . $parca[0] . ".php";
    }

    ?>

                        </div>
                    </div>

                </div>
            </div>
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Civil Bilgi Teknolojileri</span>
                    </div>
                </div>
            </footer>

        </div>
    </div>

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>


    <!-- Bootstrap core JavaScript-->
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/chart-area-demo.js"></script>
    <script src="js/demo/chart-pie-demo.js"></script>
    <script src="js/demo/custom.js"></script>
    
    <!-- Page level plugins -->
    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="js/demo/datatables-demo.js"></script>
    
</body>

</html>
<?php

}else{
    header("location:./");
}
ob_end_flush();
?>