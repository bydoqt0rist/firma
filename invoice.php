<?php


require_once "fonx/class.fonx.php";
$db = new ayar();

    if(!$_GET["print"] == ""){
        $printMd5 = $_GET["print"];
        $printDecod = explode(".",$printMd5);
        $el = base64_decode($printDecod[1]);        
        $rwOut = $db->qr("select *, DATE_FORMAT(Created_date,'%d.%m.%Y %H:%i') as tarih from ordermember where id = '$el'")->fetch(PDO::FETCH_ASSOC);
        $siparisNo = $rwOut["id"];
        $adiSoyadi = $rwOut["firstlastName"];
        $telefon = $rwOut["phone1"];
        $tarih = $rwOut["tarih"];        
        $adres = $rwOut["address"];        
        $office = $rwOut["OfficeDesc"];        
        $faturaNo = $rwOut["v3InvoiceId"];        
        $cep = $rwOut["phone1"];
        $mail = $rwOut["mailAddress"];
        $il_ilce = $rwOut["city"]." / ".$rwOut["town"];
    }else{}



?>
    
<!DOCTYPE html>
<html lang="tr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="invoice.css">
</head>

<body>
<input type="submit"  class="sil" value="Yazdır" onclick="window.print()" /> 

    <page size="A4">

    <div class="page">

        <div><img class='img' src='img/20535.png' /></div>

        <div class="clean"></div>

        <div class="musteri">
            <strong class="musteri_adi"><?=mb_strtoupper($adiSoyadi,"UTF8")?></strong>
            
        </div>
        
        <div class="clean"></div>

        <div class="card">

            

            <p class="musteri_date"><strong>Sipariş Tarihi:</strong>  <br /><?=$tarih?></p>
            
            <p class="numarasi"><strong>Fatura No :</strong> <br /><?=$faturaNo?></strong></p>
            <p class="numarasi"><strong>Sipariş No :</strong> <br /><?=$siparisNo?></strong></p>




        </div>

        <div class="card_2">


            <p class="musteri_date" style="text-align:right"><strong>Adresi  :</strong><br /><span class="clean"><?=$adres?></span></p>
            <p class="musteri_date" style="text-align:right"><strong>İl / İlçe :</strong><br /><span class="clean"><?=$il_ilce?></span></p>
            <p class="musteri_date" style="text-align:right"><strong>Telefon Numarası :</strong><br /><span class="clean">0<?=$cep?></span></p>
            <p class="musteri_date" style="text-align:right"><strong>Mağaza</strong><br /><span class="clean"><?=$office?></span></p>


        </div>

            <div class="clean"></div>
        <div class="siparis_listesi">
            <table>
                <thead>
                
                    <th>Ürün Kodu</th>
                    <th>Ürün Adı</th>
                    <th>Miktar</th>
                </thead>
                <tbody>
                    <?php
    
    $qry = $db->qr("select * from orderproduct where omid = '$el'");
    while($rws = $qry->fetch(PDO::FETCH_ASSOC)){
            echo '
            <tr>
            <td>'.$rws["productCode"].'</td>
            <td>'.$rws["productName"].'</td>
            <td>'.$rws["productTotal"].'</td>
        </tr>         
            ';
    }
                    ?>           
                </tbody>
            </table>
        </div>


        <div class="clean"></div>
        
        <div class="footer">
            <div class="container">
            <ul>
                <li style="padding-left:150px;">Civil Mağazacılık A.Ş.</li>
                <li>444 1 234</li>
            </ul>
        </div>
        </div>
        
        </div>
    </page>
</body>
</html>