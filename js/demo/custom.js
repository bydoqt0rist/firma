function modal(icon, baslik, text) {
    Swal.fire({
        icon: icon,
        title: baslik,
        text: text
    });
}
$(document).ready(function() {

    $("#montaj").click(function() {
        var option = $("#montaj").val();
        var path = window.location.pathname.split('.')[1];

        if (option == 1) {

            $.ajax({
                method: "post",
                url: "montaj.php",
                data: { durumu: 1, orderId: path },

                beforeSend: function() {
                    modal('info', 'Bekleyiniz', 'Bekleyiniz');
                },
                success: function(e) {
                    if (option == 1) {
                        modal('success', 'Başarılı', 'Sipariş Alındı' + e);
                        setTimeout(function() {
                            window.location.reload(1);
                        }, 1000);
                    }
                }
            })

        }
    });
    $("#durum").click(function() {
        var option = $("#durum").val();
        var path = window.location.pathname.split('.')[1];

        if (option == 3) {
            swal.fire({
                title: 'Kargo Takip Numarasını Giriniz',
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Tamam',

                showLoaderOnConfirm: true,
                preConfirm: (followNo) => {

                    $.ajax({
                        method: "post",
                        url: "siparis.php",
                        data: {
                            durumu: option,
                            takipNo: followNo,
                            orderId: path
                        },
                        success: function(e) {
                            modal('success', 'Başarılı', 'Takip numarası Alındı ' + e);
                            setTimeout(function() {
                                window.location.reload(1);
                            }, 1000);
                        }
                    });

                },
                allowOutsideClick: () => !Swal.isLoading()
            })
        } else if (option == 1 || option == 2) {
            $.ajax({
                method: "post",
                url: "siparis.php",
                data: { durumu: option, orderId: path },

                beforeSend: function() {
                    modal('info', 'Bekleyiniz', 'Bekleyiniz');
                },
                success: function(e) {
                    if (option == 1) {
                        modal('success', 'Başarılı', 'Sipariş Alındı');
                        setTimeout(function() {
                            window.location.reload(1);
                        }, 1000);
                    } else if (option == 2) {
                        modal('success', 'Başarılı', 'Sipariş Hazırlanıyor');
                        setTimeout(function() {
                            window.location.reload(1);
                        }, 1000);
                    }
                }
            })
        }
    });
});