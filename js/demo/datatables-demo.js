// Call the dataTables jQuery plugin
$(document).ready(function() {
    $('#dataTable').dataTable({
        "order":[],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Turkish.json"
        }
    });
});