<?php
session_Start();
require_once "fonx/class.uye.php";
$uye = new uyeOut();
if(@$_SESSION["login"] == 1){
    header("location:./dashboard.php");
}
?>
<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="utf-8" />
    <title>Giris</title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Asap+Condensed:500">
    <link href="./css/login-6.css" rel="stylesheet" type="text/css" />
    <link href="./css/style.bundle.css" rel="stylesheet" type="text/css" />
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</head>
<body class="kt-page-content-white kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">
<?php
if ($_POST) {

    $email = $_POST["email"];
    $sifre = $_POST["password"];
    $cevap = $uye->uyeSearch($email, $sifre);

    if (@$cevap == "") {
        echo "
            <script>
            Swal.fire({
                icon: 'error',
                title: 'Hata',
                text: 'Kullanıcı Adı Veya Şifreniz Hatalıdır'
              })
            </script>
        ";
    } else {

        header("refresh:1;url=dashboard.php");
        $_SESSION["ulName"] = $cevap["firstName"]." ".$cevap["lastName"];
        $_SESSION["status"] = $cevap["status"];
        $_SESSION["rutbe"] = $cevap["authority"];
        $_SESSION["login"] = 1;
        $_SESSION["id"]   = $cevap["id"];

        echo "
            <script>
            Swal.fire({
                icon: 'success',
                title: 'Başarılı',
                text: 'Başarılı Bir Şekilde Giriş Yapılmıştır'
              })
            </script>
        ";

    }

}
?>
    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root kt-page">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
                <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                    <div class="kt-login__wrapper">
                        <div class="kt-login__container">
                            <div class="kt-login__body">
                                <div class="kt-login__logo">
                                    <a href="#">
                                        <img src="img/20535.png" style="width:50%">
                                    </a>
                                </div>

                                <div class="kt-login__signin">
                                    <div class="kt-login__head">

                                    </div>
                                    <div class="kt-login__form">
                                        <form class="kt-form" method="post">
                                            <div class="form-group">
                                                <input class="form-control" type="text" required placeholder="Kullanıcı Adı Veya Mail Adresi" name="email" autocomplete="off">
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control form-control-last" required type="password" placeholder="Şifreniz" name="password">
                                            </div>
                                            <div class="kt-login__actions">
                                                <button id="kt_login_signin_submit" class="btn btn-brand btn-pill btn-elevate">Giriş Yap</button>
</div>
 </form> </div> </div> </div> </div> </div> </div> <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content" style="background-image: url(./img/bg-4.jpg);">
                                            <div class="kt-login__section">
                                                <div class="kt-login__block">
                                                    <h3 class="kt-login__title">Civil Ürün Takip Portalı</h3>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
</body>

</html>