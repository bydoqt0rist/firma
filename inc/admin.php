<?php

#Trelloda yazılıdır bütün işler.

$urlPath = $_GET["page"];
$urlExp = explode(".", $urlPath);
$urlAl = $urlExp[1];
$sonrakiFunc = $url
if (function_exists($urlAl)) {
    $urlAl();
}



function menuAtam()
{

    global $db;

        $groupMenu = $db->qr("select * from grupmenu");

        echo '
        <span>
            <h2 class="p-3">Gruplar</h2>
        </span>
        <table class="table table-striped table-inverse">
        <thead class="thead-inverse">
        <tr>
            <th>Sıra</th>
            <th>Adı</th>
            <th>Araçlar</th>
        </tr>
        </thead>
        <tbody>
        ';
        while($groupRow = $groupMenu->fetch(PDO::FETCH_ASSOC)){
            echo '

            <tr>
                <td scope="row">' . $groupRow["id"] . '</td>
                <td>'.$groupRow["name"].'</td>
                <td><a href="admin.menuatama.groupshow.' . $groupRow["id"] . '"><i class="fa fa-eye" aria-hidden="true"></i></td>
                <td></td>
            </tr>
            ';
        }
        echo '
            </tbody>
            </table>
            ';    
}



function kullanicilar(){
    global $db;
    $userTablo = $db->qr("select * from usertablo
        inner join authority on authority.id=usertablo.authority
        ");

    echo '
        <table class="table table-striped table-inverse">
        <thead class="thead-inverse">
        <tr>
            <th>Yetki Seviyesi</th>
            <th>Adı Soyadı</th>
            <th>Mail</th>
            <th>Araçlar</th>
        </tr>
        </thead>
        <tbody>
        ';

    while ($userRw = $userTablo->fetch(PDO::FETCH_ASSOC)) {
        echo '

            <tr>
                <td scope="row">' . $userRw["name"] . '</td>
                <td>' . $userRw["firstName"] . ' ' . $userRw["lastName"] . '</td>
                <td>' . $userRw["mail"] . '</td>
                <td></td>
            </tr>
            ';
    }

    echo '
        </tbody>
        </table>
        ';
}